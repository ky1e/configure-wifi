# configure-wifi
A standalone dialog-based script to select a wifi network from a list. Similar to wifi-menu found in Arch Linux, but uses systemd-networkd and wpa_supplicant instead of netctl.
# Dependencies
* systemd-networkd: part of systemd, it matches network interfaces, both wired and wireless, and manages their IP and gateway addresses along with other information.
* wpa_supplicant: used to configure wireless networks across reboots.
* iw: This is the tool used to find nearby wireless access points and get information about them. It can also connect to networks and set the regulatory domain.
crda: Necessary to honor the country= value in a wpa_supplicant configuration, this is the back-end tool that correctly sets the regulatory domain in the kernel.
